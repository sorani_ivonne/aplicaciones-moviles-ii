package com.example.plataformauptsorani.Api.Servicios;

import com.example.plataformauptsorani.ViewModels.DetalleUsuario;
import com.example.plataformauptsorani.ViewModels.EnviarNotificacion;
import com.example.plataformauptsorani.ViewModels.Login;
import com.example.plataformauptsorani.ViewModels.NumerosAleatorios;
import com.example.plataformauptsorani.ViewModels.Peticion_Noticia;
import com.example.plataformauptsorani.ViewModels.PracticaLogin;
import com.example.plataformauptsorani.ViewModels.Registro_Usuario;
import com.example.plataformauptsorani.ViewModels.UsuarioGet;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface Servicio_Peticion {
    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> registrarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    //@FormUrlEncoded
    @POST("api/numAlea")
    Call<NumerosAleatorios> numerosAleatorios();

    @FormUrlEncoded
    @POST("api/login")
    Call<Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<PracticaLogin> getLoginPractica(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/detallesUsuario")
    Call<DetalleUsuario> getDetalles(@Field("usuarioId") int id);

    @GET("api/todasNot")
    Call<Peticion_Noticia> getNoticias();

    @POST("api/todosUsuarios")
    Call<UsuarioGet> getUsuariosPractica();

    @FormUrlEncoded
    @POST("api/crearNotUsuario")
    Call<EnviarNotificacion> enviarNota(@Field("usuarioId") String id, @Field("titulo") String titulo, @Field("descripcion") String descripcion);
}
