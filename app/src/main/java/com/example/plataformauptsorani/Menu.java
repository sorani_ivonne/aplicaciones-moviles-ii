package com.example.plataformauptsorani;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Menu extends AppCompatActivity {
    Button user, notas, nota;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        notas = (Button) findViewById(R.id.button2);
        user = (Button) findViewById(R.id.button3);
        nota = (Button) findViewById(R.id.nota);
        user.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, PantallaUsuarios.class);
                startActivity(intent);
            }
        });
        notas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, PantallaNoticias.class);
                startActivity(intent);
            }
        });
        nota.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Menu.this, PantallaNotificacion.class);
                startActivity(intent);
            }
        });
    }
}
