package com.example.plataformauptsorani;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.plataformauptsorani.Api.Api;
import com.example.plataformauptsorani.Api.Servicios.Servicio_Peticion;
import com.example.plataformauptsorani.ViewModels.EnviarNotificacion;
import com.example.plataformauptsorani.ViewModels.Registro_Usuario;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaNotificacion extends AppCompatActivity {

    public Button registrar;
    public EditText user, pass1, pass2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_notificacion);
        user = (EditText) findViewById(R.id.iduser);
        pass1 = (EditText) findViewById(R.id.titulo);
        pass2 = (EditText) findViewById(R.id.descripcion);
        registrar = (Button) findViewById(R.id.registrar);
        String hola ="";
        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!user.getText().toString().isEmpty() && !pass1.getText().toString().isEmpty() && !pass2.getText().toString().isEmpty()){
                        Api api = new Api();
                        Servicio_Peticion service = api.getApi(PantallaNotificacion.this).create(Servicio_Peticion.class);
                        Call<EnviarNotificacion> registrarCall =  service.enviarNota(user.getText().toString(),pass1.getText().toString(), pass2.getText().toString());
                        registrarCall.enqueue(new Callback<EnviarNotificacion>() {
                            @Override
                            public void onResponse(Call<EnviarNotificacion> call, Response<EnviarNotificacion> response) {
                                EnviarNotificacion peticion = response.body();
                                if(response.body() == null){
                                    Toast.makeText(PantallaNotificacion.this, "Ocurrio un Error, intentalo más tarde", Toast.LENGTH_LONG).show();
                                    return;
                                }
                                if(peticion.estado == "true"){

                                    Toast.makeText(PantallaNotificacion.this, "Datos Enviados", Toast.LENGTH_LONG).show();
                                }else{
                                    Toast.makeText(PantallaNotificacion.this, peticion.detalle, Toast.LENGTH_LONG).show();
                                }
                                startActivity(new Intent(PantallaNotificacion.this,Menu.class));
                            }
                            @Override
                            public void onFailure(Call<EnviarNotificacion> call, Throwable t) {
                                Toast.makeText(PantallaNotificacion.this, "Error :(", Toast.LENGTH_SHORT).show();
                            }
                        });
                } else{
                    Toast.makeText(PantallaNotificacion.this, "Campos vacíos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
