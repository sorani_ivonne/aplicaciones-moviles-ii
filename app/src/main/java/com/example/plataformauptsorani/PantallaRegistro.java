package com.example.plataformauptsorani;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.plataformauptsorani.Api.Api;
import com.example.plataformauptsorani.Api.Servicios.Servicio_Peticion;
import com.example.plataformauptsorani.ViewModels.Registro_Usuario;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PantallaRegistro extends AppCompatActivity {

    public Button registrar;
    public EditText user, pass1, pass2;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pantalla_registro);
        user = (EditText) findViewById(R.id.user);
        pass1 = (EditText) findViewById(R.id.pass1);
        pass2 = (EditText) findViewById(R.id.pass2);
        registrar = (Button) findViewById(R.id.registrar);
        String hola ="";
        registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!user.getText().toString().isEmpty() && !pass1.getText().toString().isEmpty() && !pass2.getText().toString().isEmpty()){
                    if (pass1.getText().toString().equals(pass2.getText().toString())){
                        Api api = new Api();
                        Servicio_Peticion service = api.getApi(PantallaRegistro.this).create(Servicio_Peticion.class);
                        Call<Registro_Usuario> registrarCall =  service.registrarUsuario(user.getText().toString(),pass2.getText().toString());
                        registrarCall.enqueue(new Callback<Registro_Usuario>() {
                            @Override
                            public void onResponse(Call<Registro_Usuario> call, Response<Registro_Usuario> response) {
                                Registro_Usuario peticion = response.body();
                                if(response.body() == null){
                                    Toast.makeText(PantallaRegistro.this, "Ocurrio un Error, intentalo más tarde", Toast.LENGTH_LONG).show();
                                    return;
                                }
                                if(peticion.estado == "true"){
                                    startActivity(new Intent(PantallaRegistro.this,PantallaLogin.class));
                                    Toast.makeText(PantallaRegistro.this, "Datos Registrador", Toast.LENGTH_LONG).show();
                                }else{
                                    Toast.makeText(PantallaRegistro.this, peticion.detalle, Toast.LENGTH_LONG).show();
                                }
                            }
                            @Override
                            public void onFailure(Call<Registro_Usuario> call, Throwable t) {
                                Toast.makeText(PantallaRegistro.this, "Erro :(", Toast.LENGTH_SHORT).show();
                            }
                        });
                    } else {
                        Toast.makeText(PantallaRegistro.this, "Contraseñas diferentes", Toast.LENGTH_SHORT).show();
                    }
                } else{
                    Toast.makeText(PantallaRegistro.this, "Campos vacíos", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}
